export const suma = (a, b) => {
    return a+b;
}

export const resta = (a, b) => {
    return a-b;
}

export const checkInteger = (value) => {
    if(value % 1 === 0){
        return true;
    }
    return false;
}

//Escribe una función que tome 2 numeros a y b como argumentos
//Si a es menor que b entonces divide por b
//De lo contrario multiplica ambos numeros
//Retorna el resultado

//Debería regresar 8 al pasar 4 y 2 como argumentos
//Debería regresar 10 al pasar 10 y 100 como argumentos
//Debería regresar 0.5 si se pasa 1 y 2 como argumentos
//Debería regresar 9 si se pasa 3 y 3 como argumentos


export const myFunction = (a, b)  => {
    if(a < b){
        return a/b;
    }
    return a*b;
}
