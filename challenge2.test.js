import { getPokemons } from "./services.js";

test("Debería de regresar un arreglo al hacer la petición a la pokeapi", async () => {
    //Arrange

    //Act
    let results = await getPokemons();
    //Assert
    expect(results).toEqual(expect.any(Array));
});

test("Debería de regresar el pokemon ivysaur", async () => {
    //Arrange
    let pokemon = "ivysaur";
    //Act
    let results = await getPokemons();
    //Assert
    expect(results).toEqual(
        expect.arrayContaining([
            { name: pokemon, url: expect.any(String) },
        ])
    );
});