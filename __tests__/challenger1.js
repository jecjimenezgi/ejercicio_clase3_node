//Escribir una función que revise si un valor pasado como argumento es un numero entero

// Debería de regresar true al probar el valor 5
// Debería de regresar false al probar el valor "javascript"
// Debería de regresar false al probar el valor 5.8
// Debería de regresar false al probar el valor { a: 1 }
import { checkInteger } from "../math.js";
import { myFunction } from "../math.js";

describe("checkInteger", () => {
  test("Debería de regresar true al probar el valor 5", () => {
    //AAA

    //Arrange (Ordenar)
    let value = 5;
    //Act (Actuar)
    let result = checkInteger(value);
    //Assert (Afirmar)
    expect(result).toBe(true);
  });

  test("Debería de regresar false al probar el valor javascript", () => {
    //AAA

    //Arrange (Ordenar)
    let value = "javascript";
    //Act (Actuar)
    let result = checkInteger(value);
    //Assert (Afirmar)
    expect(result).toBe(false);
  });

  test("Debería de regresar false al probar el valor 5.8", () => {
    //AAA

    //Arrange (Ordenar)
    let value = 5.8;
    //Act (Actuar)
    let result = checkInteger(value);
    //Assert (Afirmar)
    expect(result).toBe(false);
  });

  test("Debería de regresar false al probar el valor { a: 1 }", () => {
    //AAA

    //Arrange (Ordenar)
    let value = { a: 1 };
    //Act (Actuar)
    let result = checkInteger(value);
    //Assert (Afirmar)
    expect(result).toBe(false);
  });
});

//Escribe una función que tome 2 numeros a y b como argumentos
//Si a es menor que b entonces divide por b
//De lo contrario multiplica ambos numeros
//Retorna el resultado

//Debería regresar 8 al pasar 4 y 2 como argumentos
//Debería regresar 10 al pasar 10 y 100 como argumentos
//Debería regresar 0.5 si se pasa 1 y 2 como argumentos
//Debería regresar 9 si se pasa 3 y 3 como argumentos

describe("myFunction", () => {
  test("Debería regresar 8 al pasar 4 y 2 como argumentos", () => {
    //Arrange (Ordenar)
    let a = 4;
    let b = 2;
    //Act (Actuar)
    let result = myFunction(a, b);
    //Assert (Afirmar)
    expect(result).toBe(8);
  });

  test("Debería regresar 10 al pasar 10 y 100 como argumentos", () => {
    //Arrange (Ordenar)
    let a = 10;
    let b = 100;
    //Act (Actuar)
    let result = myFunction(a, b);
    //Assert (Afirmar)
    expect(result).toBe(0.1);
  });

  test("Debería regresar 0.5 si se pasa 1 y 2 como argumentos", () => {
    //Arrange (Ordenar)
    let a = 1;
    let b = 2;
    //Act (Actuar)
    let result = myFunction(a, b);
    //Assert (Afirmar)
    expect(result).toBe(0.5);
  });

  test("Debería regresar 9 si se pasa 3 y 3 como argumentos", () => {
    //Arrange (Ordenar)
    let a = 3;
    let b = 3;
    //Act (Actuar)
    let result = myFunction(a, b);
    //Assert (Afirmar)
    expect(result).toBe(9);
  });
});
