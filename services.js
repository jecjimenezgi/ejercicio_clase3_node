import axios from "axios";

export const getPokemons = async() => {
    try{
        let {data} = await axios.get("https://pokeapi.co/api/v2/pokemon");
        return data.results;
    }catch(e){
        console.log(e);
    }
}